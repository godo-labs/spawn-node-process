// console.error('teste');
const AdmZip = require('adm-zip');
// const fs = require('fs')
const storagePath = './storage'
const uuid = require('uuid');
var ArgumentParser = require('argparse').ArgumentParser;

var parser = new ArgumentParser({
  version: '0.0.1',
  addHelp:true,
  description: 'Argparse example'
});

parser.addArgument(
    [ '-f', '--files' ],
    {
        help: 'Files'
    }
);

parser.addArgument(
    [ '-b', '--batch' ],
    {
        help: 'Batch id'
    }
);

args = parser.parseArgs();

let files = args.files.split(',');

// fs.readdirSync(storagePath).forEach( function(file) {
//     // console.log(file);
//     // zip.addLocalFile(storagePath, uuid.v4());
//     let filepath = storagePath + '/' + file;
//     files.push(filepath);
// });

if(files.length > 0)
{
    let zip = new AdmZip();
    
    files.forEach(function(file){
        zip.addLocalFile(storagePath + '/' + file);
    });

    zip.writeZip('./zipped/' + uuid.v4() + '.zip');

    // files.forEach(function(file){
    //     fs.unlink(file, (err)=>{});
    // });
}