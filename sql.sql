CREATE TABLE public.queue
(
  id bigint NOT NULL DEFAULT nextval('queue_id_seq'::regclass),
  files json,
  batch bigint NOT NULL DEFAULT nextval('queue_batch_seq'::regclass),
  processing boolean
)

INSERT INTO public.queue(files, processing)
VALUES ('["lote1_arquivo1.txt","lote1_arquivo2.txt"]', true);

INSERT INTO public.queue(files, processing)
VALUES ('["lote2_arquivo1.txt","lote2_arquivo2.txt"]', true);