const { Client } = require('pg');

module.exports = function queue(success, error)
{
    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        database: 'sys3',
        password: 'postgres',
        port: 5432
    });
    
    client.connect();
    
    client.query('SELECT * from queue')
    .then((res) =>{
        // console.log('Resultado consulta banco: ', res.rows[0]); // Hello World!
        success(res.rows);
        client.end();
    })
    .catch((err) => {
        console.log('Erro consulta banco: ', err.stack); // Hello World!    
    });
}