const {spawn} = require('child_process');
const http = require('http');
const httpServer = http.createServer();
const queue = require('./queue');

httpServer.on('request', (req, res) => {
    queue((items) => {
        items.forEach((item) => {
            let child = spawn('node', ['zipbatch.js', `-f=${item.files}`]);

            child.on('error', function(error){
                console.log(`Erro ao executar o compactador de lotes: ${error}`); 
            });

            child.on('exit', function(code, signal){
                console.log(`O processo filho saiu com o código ${code} e sinal ${signal}`); 
            });

            child.stdout.on('data', (data) => {
                console.error(`Mensagem do filho:\n${data}`);
            });

            child.stderr.on('data', (data) => {
                console.error(`Erro do filho:\n${data}`);
            });
            res.end('ok');
        });
    });
});

httpServer.listen(5000, function()
{
    process.send('ready');
});